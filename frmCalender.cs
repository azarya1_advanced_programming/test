using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
////
namespace HWLesson19
{
    public partial class frmCalender : Form
    {
        string user;
        Dictionary<string, string> birthDays = new Dictionary<string, string>();
        public string userNameIn
        {
            get { return user; }
            set { user = value; }
        }

        public frmCalender()
        {
            InitializeComponent();
        }

        private void frmCalender_Load(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader(@"" + user + "BD" + ".txt");

            string line;
            while (!sr.EndOfStream)
            {
                line = sr.ReadLine();
                birthDays[line.Split(',')[0]] = line.Split(',')[1];
            }
            string selectedDate = calCalendar.SelectionRange.Start.ToShortDateString();
            lblMessageLabel.Text = "";
			
			
            foreach (KeyValuePair<string, string> cell in birthDays)//lop in all the array.
            {
                if (cell.Value == selectedDate)
                {
                    lblMessageLabel.Text = "Its " + cell.Key + "'s birthday!";
                }
            }
            if (lblMessageLabel.Text == "")
            {
                lblMessageLabel.Text = "no birhtday ):";
            }
        }

        private void calCalendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            string selectedDate = calCalendar.SelectionRange.Start.ToShortDateString();
            lblMessageLabel.Text = "";
            foreach (KeyValuePair<string, string> cell in birthDays)
            {
                if (cell.Value == selectedDate)
                {
                    lblMessageLabel.Text = "Its " + cell.Key + "'s birthday!";
                }
            }
            if (lblMessageLabel.Text == "")
            {
                lblMessageLabel.Text = "not birhtday ):";
            }
        }
    }
}

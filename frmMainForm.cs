using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
////

namespace HWLesson19
{
    public partial class frmMainForm : Form
    {
        Dictionary<string, string> users = new Dictionary<string, string>();
        public frmMainForm()
        {
            InitializeComponent();
        }

        private void frmMainForm_Load(object sender, EventArgs e)
        {
            StreamReader sr = new StreamReader(@".txt");
            string line;
            while (!sr.EndOfStream)
            {
                line = sr.ReadLine();
                users[line.Split(',')[0]] = line.Split(',')[1];
            }
            sr.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string userName = txtUserName.Text;
            string password = txtPassword.Text;

            try
            {
                if (users[userName] == password)
                {
                    frmCalender frmNewForm = new frmCalender();
                    frmNewForm.userNameIn = userName;
                    frmNewForm.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("not good password");
                    txtPassword.Text = "";
                    txtUserName.Text = "";
                }
            }
            catch (KeyNotFoundException)
            {
                MessageBox.Show("You are not in users!");
                txtPassword.Text = "";
                txtUserName.Text = "";
            }

        }
    }
}
